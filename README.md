**JOKES PROJECT**


- JDK 11.0.6
- Apache Maven 3.6.3

---
## Zadatak

1. /documents/CROZ_Java zadatak_T.pdf

---

## Database resources

1. Postgres database
2. Database name: jokes
2. Database user/data source owner admin/admin
3. Database migration tool Flyway
4. Flyway migration file /documents/V1__data.sql
5. Application properties configuration /resources/application.properties
---

## URLs

1. All jokes table - /
2. Add new joke - /new

---
