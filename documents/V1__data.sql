CREATE TABLE category
(
    id integer NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    name character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT category_pkey PRIMARY KEY (id)
);

ALTER TABLE public.category OWNER to admin;

CREATE TABLE joke
(
    id integer NOT NULL GENERATED BY DEFAULT AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    content character varying(255) COLLATE pg_catalog."default",
    dislikes integer NOT NULL,
    likes integer NOT NULL,
    category_id integer,
    CONSTRAINT joke_pkey PRIMARY KEY (id),
    CONSTRAINT fk2kq8grpaolu7fi816anu1dqk4 FOREIGN KEY (category_id)
        REFERENCES public.category (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

ALTER TABLE public.joke OWNER to admin;

INSERT INTO category(name) VALUES ('Chuck Norris');
INSERT INTO category(name) VALUES ('Škola');
INSERT INTO category(name) VALUES ('Mujo');

INSERT INTO joke(content, dislikes, likes, category_id)
	VALUES ('Zašto je Chuck Norris najjači? Zato što vježba dva dana dnevno', 10, 72, 1);
INSERT INTO joke(content, dislikes, likes, category_id)
	VALUES ('Pita nastavnica hrvatskog jezika mladog osnovnoškolca:Reci ti meni što su to prilozi? Prilozi su: ketchup, majoneza, luk, salata...', 40, 80, 2);
INSERT INTO joke(content, dislikes, likes, category_id)
	VALUES ('Pričaju dvije gimnazijalke: Nema mi roditelja doma ovaj vikend! Bože, pa koja si ti sretnica! Možeš učiti naglas!', 2, 25, 2);
INSERT INTO joke(content, dislikes, likes, category_id)
	VALUES ('Došao Mujo u pizzeriju i naručio pizzu. Konobar ga upita: Želite da vam izrežem pizzu na 6 ili 12 komada? Ma na 6 komada, nema šanse da pojedem 12.', 9, 32, 3);