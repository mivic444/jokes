package hr.mivic.jokes.models;

import org.springframework.boot.context.properties.bind.DefaultValue;

import javax.persistence.*;

@Entity
public class Joke {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String content;
    private int likes;
    private int dislikes;
    @ManyToOne()
    @JoinColumn(name = "categoryId")
    private Category category;

    protected Joke() {};

    public Joke(String content, Category category) {
        this.content = content;
        this.likes = 0;
        this.dislikes = 0;
        this.category = category;
    }

    @Override
    public String toString() {

        return String.format(
                "Joke[id=%d, content='%s', likes='%d', dislikes='%d']",
                id, content, likes, dislikes);
    }

    public int getVotingDifference() {

        return this.likes - this.dislikes;
    }
    public void addLike(){

        this.likes += 1;
    }

    public void addDislike() {

        this.dislikes += 1;
    }

    public int getId() {

        return id;
    }

    public String getContent() {

        return content;
    }

    public int getLikes() {

        return likes;
    }

    public int getDislikes() {

        return dislikes;
    }

    public Category getCategory() {

        return category;
    }
}
