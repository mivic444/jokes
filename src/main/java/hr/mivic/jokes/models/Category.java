package hr.mivic.jokes.models;

import javax.persistence.*;


@Entity
public class Category {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String name;

    protected Category() {};

    public Category(String name){

        this.name = name;
    }

    @Override
    public String toString() {

        return String.format("JokeCategory[id=%d, name='%s']", id, name);
    }

    public int getId() {

        return id;
    }

    public String getName() {

        return name;
    }
}
