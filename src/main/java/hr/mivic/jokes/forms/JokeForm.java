package hr.mivic.jokes.forms;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class JokeForm {

    @NotNull
    @Size(min=5, max=500, message = "Please write 5 to 500 characters")
    private String content;
    @Min(value = 1, message = "Please select joke category")
    private int categoryId;

    public String getContent() {

        return content;
    }

    public void setContent(String content) {

        this.content = content;
    }

    public int getCategoryId() {

        return categoryId;
    }

    public void setCategoryId(int categoryId) {

        this.categoryId = categoryId;
    }
}
