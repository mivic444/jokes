package hr.mivic.jokes.services;

import hr.mivic.jokes.models.Joke;
import hr.mivic.jokes.repositories.JokeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class JokeService {

    @Autowired
    private JokeRepository jokeRepository;

    public List<Joke> getJokesByVotingDiffAsc() {

        List < Joke > jokes = jokeRepository.findAll();
        
        Comparator< Joke > compareByVotingDifference = (Joke o1, Joke o2) ->
                Integer.compare(o1.getVotingDifference(), o2.getVotingDifference());
        Collections.sort(jokes, compareByVotingDifference.reversed());

        return jokes;
    }

    public void likeJoke(int jokeId) {

        Joke joke = jokeRepository.findById(jokeId);
        joke.addLike();
        jokeRepository.save(joke);
    }

    public void dislikeJoke(int jokeId) {

        Joke joke = jokeRepository.findById(jokeId);
        joke.addDislike();
        jokeRepository.save(joke);
    }
}
