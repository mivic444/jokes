package hr.mivic.jokes.repositories;

import hr.mivic.jokes.models.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category, Integer> {

    List<Category> findAll();

    Category findById(int id);
}
