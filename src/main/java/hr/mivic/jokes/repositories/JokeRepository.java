package hr.mivic.jokes.repositories;

import hr.mivic.jokes.models.Joke;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JokeRepository extends CrudRepository<Joke, Integer> {

    List<Joke> findAll();

    Joke findById(int id);

}
