package hr.mivic.jokes.controllers;

import hr.mivic.jokes.forms.JokeForm;
import hr.mivic.jokes.models.Category;
import hr.mivic.jokes.models.Joke;
import hr.mivic.jokes.repositories.CategoryRepository;
import hr.mivic.jokes.repositories.JokeRepository;
import hr.mivic.jokes.services.JokeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
public class JokeController {

    @Autowired
    private JokeRepository jokeRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private JokeService jokeService;

    @GetMapping("/")
    public String getAllJokes(Model model) {

        model.addAttribute("jokes", jokeService.getJokesByVotingDiffAsc());
        return "allJokes";
    }

    @PostMapping(value = "/", params = "likeId")
    public String likeJoke(@RequestParam int likeId) {

        jokeService.likeJoke(likeId);
        return "redirect:/";
    }

    @PostMapping(value = "/", params = "dislikeId")
    public String dislikeJoke(@RequestParam int dislikeId) {

        jokeService.dislikeJoke(dislikeId);
        return "redirect:/";
    }

    @GetMapping("/new")
    public String newJokeForm(JokeForm jokeForm, Model model) {

        model.addAttribute("jokeCategories", categoryRepository.findAll());
        return "newJoke";
    }

    @PostMapping("/new")
    public String createNewJoke(@Valid JokeForm jokeForm, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("jokeCategories", categoryRepository.findAll());
            return "newJoke";
        }

        String jokeContent = jokeForm.getContent();
        int categoryId = jokeForm.getCategoryId();
        Category category = categoryRepository.findById(categoryId);
        Joke joke = new Joke(jokeContent, category);

        jokeRepository.save(joke);
        return "newJokeResult";
    }
}